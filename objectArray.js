/*
	objects can also be grouped into an array
*/

let users = [

	{
		username:"mike9900",
		email: "michaeljames@gmail.com",
		password: "mikecutie1999",
		isAdmin: false
	},
	{
		username: "justin89",
		email: "justintlake@gmail.com",
		password: "iamnsync00",
		isAdmin: true
	}

];

console.log(users[0]);
console.log(users[1]);
console.log(users[1].email);
console.log(users[0].username);

// add new obj at end of array

users.push({
	username: "abbeyarcher",
	email: "abbeyarrows@gmail.com",
	password: "bowandabbey",
	isAdmin: false
})

console.log(users);

let newUser = {
	username: "hanna1993",
	email: "hannafight@gmail.com",
	password: "fighting1993",
	isAdmin: false,
}

users.push(newUser);
console.log(users);

class User {
	constructor(username,email,password){
		this.username = username;
		this.email = email;
		this.password = password;
		this.isAdmin = false;
	}
}

let user1 = new User("kateduchess","nottherealone@gmail.com","imnotroyalty");
users.push(user1);

console.log(users);

//Mini-Activity
let user2 = new User("usherAko", "usherAko@yahoo.com","secret", false);
users.push(user2);

let user3 = new User("userAko","userAko@yahoo.com","notUser", false);
users.push(user3);

console.log(users);

users.push(new User("newuser1","newuseremail@gmail.com","newuser8"));
console.log(users);

// Register function
/* When creating a function which will receive data as an argument,
always console log the parameters first. */

function register(username,email,password){
	console.log(username);
	console.log(email);
	console.log(password);

	if (username.length >8 && email.length > 8 && password.length > 8) {
		users.push({
	username: username,
	email: email,
	password: password,
	isAdmin: false
})
	} else {alert("Details provided too short. username, email, and password must be more than 8 characters.")}
// register function should be able to push a new user object in our array:
// console.log(users); console.log() everything!

}

register("jeffrey1888","immortal1@gmail.com","awesomemortal1");
console.log(users);

// Login function

/* find () 
-check our array if there is a user which matches email and password provided/input in function
-like forEach and map, will iterate/loop over each item in array then return a condition.
If condition returned results to be true, find() will return item currently iterated/looped over.
*console.log parameters first to check if it's received
*/

function login(emailInput,pwInput){
	console.log(emailInput);
	console.log(pwInput);

	/* Anonymous function in the find() method receives current 
	item being looped over or iterated*/
	let foundUser = users.find(user=>{
		console.log(user.email);
		console.log(user.password);
		// check user's email & pw if matches our emailInput and pwIpunt
		return user.email === emailInput && user.password === pwInput;
	})

	// console.log(foundUser);

	if (foundUser !== undefined){
		console.log("Thank you for logging in.")
	} else {
		console.log("Invalid Credentials. No User Found.")
	}
}
 login("immortal1@gmail.com","awesomemortal");





