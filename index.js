let assets = [];

const addAsset = (id,name,description,stock,isAvailable,dateAdded)=>{assets.push(
	{id: id,
	name: name,
	description: description,
	stock: stock,
	isAvailable: isAvailable,
	dateAdded: dateAdded
	});
}

addAsset("item-1","Construction Crane","Heavy lifting machine",5,true,"July 7, 2019");
addAsset("item-2","Backhoe","Heavy Machinery",2,true,"May 1, 2004");
console.log(assets);

// JSON - Javascript Object Notation
/* JSON is a string
JS Object is an object
JSON Keys are surrounded by double quotes
JSON extra comma at end will become error
*/
let jsonSample = `{
	"sampleKey1": "valueA",
	"sampleKey2": "valueB",
	"sampleKey3": 1,
	"sampleKey4": true
}`

console.log(typeof jsonSample);

let jsonConvert = JSON.parse(jsonSample);
console.log(typeof jsonConvert);
console.log(jsonConvert);

/* JSON is used in other programming languages
this is why it is specified as JavaScript Object Nottation
there are some saved in 
*/

let batches = [
	{
		batch:`Batch 152`
	},
	{
		batch:`Batch 156`
	}
];
// This method will return JSON format out of the obj that we pass as an argument.
let batchesJSON = JSON.stringify(batches);
console.log(batchesJSON);

let data = {
	name: "Katniss",
	age: 20,
	address: {
		city: "Kansas City",
		state: "Kansas"
	}
}

let dataJSON = JSON.stringify(data);
console.log(dataJSON);

/* JSON.stringify is commonly used when trying to pass data from one
application to another via HTTP requests. HTTP requests are a request
for data between the client(browser/page/app) and a server.
*/

let data2 = {
	username: "saitamaOPM",
	password: "onepuuuuunch",
	isAdmin: true
}

let data3 = {
	username: "lightYagami",
	password: "notKiraDefinitely",
	isAdmin: false
}

let data4 = {
	username: "Llawliett",
	password: "yagamiiskira07",
	isAdmin:false
}

let data2JSON = JSON.stringify(data2);
let data3JSON = JSON.stringify(data3);
let data4JSON = JSON.stringify(data4);
let jsonArray = JSON.stringify(assets);

console.log(data2JSON);
console.log(data3JSON);
console.log(data4JSON);
console.log(jsonArray);

jsonArray.pop();

